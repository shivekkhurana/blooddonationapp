export default {
  http: {
    host: '0.0.0.0',
    port: 3000
  },
  mongo: 'mongodb://localhost/blood_donation_management'
};
