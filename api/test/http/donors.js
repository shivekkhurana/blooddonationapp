import {expect, should} from 'chai';
import supertest from 'supertest';

should();
const api = supertest('http://localhost:3000');
const donorIds = [];

describe('Donors', () => {
  it('should give healthy status at GET /', () => {
    api
      .get('/')
      .end((err, res) => {
        res.status.should.equal(200);
        expect(res.body).to.have.property('status').that.is.a.string;
      });
  });

  it('should give a list and count of donors at GET /donors', (done) => {
    api
      .get('/donors')
      .end((err, res) => {
        expect(res.body).to.have.property('count').that.is.a.number;
        expect(res.body.donors).to.be.an('array');
        res.body.donors.forEach((donor) => {
          expect(donor).to.have.property('_id').that.is.a.string;
        });
        done();
      })
    ;
  });

	it('should create a new donor at POST /donors', (done) => {
		api
      .post('/donors')
      .send({
      	firstName: 'Best',
				lastName: 'Ever',
				email: 'deepak@crossover.com',
				contactNumber: '9999999999',
				bloodGroup: 'B+',
				geo: {lat: 40.3532532, lng: 16.3432532},
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }

        // Api Works
        res.status.should.equal(200);
        res.body.donor.email.should.equal('deepak@crossover.com');
        expect(res.body.donor).to.have.property('_id').that.is.a.string;

        donorIds.push(res.body.donor._id);
        done();
      })
    ;
	});

  it('should throw error, if credentials are missing at POST /donors', (done) => {
    api
      .post('/donors')
      .send({
        firstName: 'Best',
        lastName: 'Ever',
        contactNumber: '999999999',
        bloodGroup: 'B+',
        geo: {lat: 56.3532532, lng: 28.3432532},
      })
      .end((err, res) => {
        // Api Works
        res.status.should.equal(400);
        expect(res.body.errors).to.have.property('email');
        done();
      })
    ;
  });

  it('should delete the donor at DELETE /donor/:donorId', (done) => {
    donorIds.forEach((id) => {
      api
      .delete(`/donors/${id}`)
      .end((err, res) => {
        // Api Works
        res.status.should.equal(200);
        expect(res.body).to.have.property('msg');
        done();
      })
    ;
    }); 
  });
});