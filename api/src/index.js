import server from 'src/http';
import express from 'express';
import config from 'config';
import mongoose from 'mongoose';

import {printIp, handleAsyncExceptions} from 'src/util';

// promisify mongoose
mongoose.Promise = Promise;

// connect to mongo db
mongoose.connect(config.mongo, { server: { socketOptions: { keepAlive: 1 } } });
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${config.mongo}`);
});


async function run() {
  server.listen(config.http.port, config.http.host, () => {
    console.log(`App Running on http://${config.http.host}:${config.http.port}\n`);
    printIp();
  });
}

export default run;

if (require.main === module) {
  handleAsyncExceptions();
  run(...process.argv.slice(2));
}
