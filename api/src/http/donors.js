import http from 'http';
import express from 'express';
import ioInit from 'socket.io';
import {isEmpty} from 'lodash';

import Donor from 'src/models/donor';

const app = express();
const server = http.createServer(app)
const io = ioInit(server);

const reqTransformer = (req) => {
	return {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		contactNumber: req.body.contactNumber,
		bloodGroup: req.body.bloodGroup,
		ipAddress: req.ip,
		geo: {lat: req.body.geo ? +(req.body.geo.lat) : undefined , lng: req.body.geo ? +(req.body.geo.lng) : undefined},
	};
};

const loadDonor = (req, res, next) => {
  Donor
  	.get(req.params.donorId)
  	.then((donor) => {
    	req.donor = donor;
    	return next();
  	})
  	.catch((e) => {
  		return res.status(404).send({msg: 'Donor does not exist.'})
  	})
  ;
}

app.get('/', async (req, res) => {
	// const donors = ! isEmpty(req.query.coords) && ! isNaN(req.query.coords[0])? await Donor.inGeographicRange(req.query.coords) : await Donor.all();
	const donors = await Donor.all();
	const count = await Donor.count();
	return res.status(200).send({donors, count});
});

app.get('/:donorId', async (req, res) => {
	Donor
		.get(req.params.donorId)
		.then((donor) => res.status(200).send(donor))
		.catch((e) => res.status(404).send({msg: 'Donor with given id does not exists.'}))
	;
});

app.post('/', async	(req, res) => {
	const donor = new Donor(reqTransformer(req));

	donor
		.save()
		.then(() => {
			req.io.emit('newDonorAdded', donor);
			return res.status(200).send({donor, msg: 'Donor created successfully.'});	
		})
		.catch(({errors}) => {
			const eRes = {};
			Object.keys(errors).map((e) => {
				const path = errors[e].properties ? errors[e].properties.path : errors[e].reason.path;
				eRes[path] = errors[e].message.replace('Path', '').replace(/`/g, '');
				return null;
			});
			return res.status(400).send({errors: eRes});
		})
	;
});

app.put('/:donorId', loadDonor, async (req, res) => {
	// add validations here !!
	Donor
		.findOneAndUpdate({_id: req.donor._id}, reqTransformer(req), {runValidators: true, new: true})
		.then((donor) => {
			req.io.emit('donorUpdated', donor);
			return res.status(200).send({msg: 'Donor info updated.', donor})
		})
		.catch(({errors}) => {
			const eRes = {};
			Object.keys(errors).map((e) => {
				eRes[errors[e].properties.path] = errors[e].message;
				return null;
			});
			return res.status(400).send({errors: eRes})
		})
	;
});

app.delete('/:donorId', loadDonor, async (req, res) => {
	const donor = req.donor;
	donor
		.remove()
		.then(() => {
			req.io.emit('donorDeleted', donor);
			return res.status(200).send({donor, msg: 'Donor info deleted.'})		
		})
		.catch((e) => res.status(400).send({msg: 'Unable to delete donor.'}))
	;
});

export default app;
