import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import errorhandler from 'errorhandler';
import cors from 'cors';
import donors from 'src/http/donors';
import ioInit from 'socket.io';

const app = express();
const server = http.createServer(app)
const io = ioInit(server);

io.on('connect', (socket) => {
	socket.emit('notification', {msg: 'Connected to real time server successfully.'})
});

// bind io to req
app.use(function(req, res, next) { 'use strict'; req.io = io; next(); });

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// parse application/json
app.use(bodyParser.json({limit: '50mb'}));

// parse multipart-form-data
app.use(cors());

if (process.env.NODE_ENV === 'production') {
  // trust proxy in production from local nginx front server
  app.set('trust proxy', 'loopback');
}

app.get('/', (req, res) => {
	return res.status(200).send({msg: 'blood-donor-management api v1', status: 'healthy', ip: req.ip})
});

app.use('/donors', donors);

// catch all route
app.all('*', (req, res) => {
  res.status(404).send({msg: 'not found'});
});

export default server;
