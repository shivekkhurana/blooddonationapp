import mongoose from 'mongoose';
import {isString} from 'lodash';
import {isEmail, isContactNumber} from 'src/util';

const schema = new mongoose.Schema({
	firstName: {type: String, required: true},
	lastName: {type: String, required: true},
	email: {type: String, required: true},
	contactNumber: {type: String, required: true},
	bloodGroup: {type: String, required: true},
	ipAddress: {type: String, required: true},
	geo: {lat: {type: Number, required: true}, lng: {type: Number, required: true}},
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now },
});

// validations
schema.path('firstName').validate(firstName => isString(firstName), 'First name is required and should be a string.');
schema.path('lastName').validate(lastName => isString(lastName), 'Last name is required and should be a string.');
schema.path('email').validate(email => isEmail(email), 'Email should be valid.');
schema.path('contactNumber').validate(contactNumber => isContactNumber(contactNumber), 'Contact number should be a valid number.');

schema.statics = {
	async all() {
    return this.find().exec();
  },
  async inGeographicRange(coords) {
    const xmin = +(coords[0]);
    const ymin = +(coords[1]);
    const xmax = +(coords[2]);
    const ymax = +(coords[3]);

    // for esri lng=x, lat=y
    // http://gis.stackexchange.com/questions/11626/does-y-mean-latitude-and-x-mean-longitude-in-every-gis-software

    return this
      .find({})
      .where('geo.lng').gt(xmin).lt(xmax)
      .where('geo.lat').gt(ymin).lt(ymax)
      .exec() 
    ;
  },
  async get(id) {
    return this
    	.findById(id)
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
        return Promise.reject();
      })
    ;
  },
};

export default mongoose.model('Donor', schema);