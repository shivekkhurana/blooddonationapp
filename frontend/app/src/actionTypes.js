const actionTypes = {
  app: {
    clearErrors: 'appClearErrors',
    notify: 'appNotify',
    clearNotification: 'appClearNotification'
  },
  donors: {
    create: {
      success: 'donorsCreateSuccess',
      error: 'donorsCreateError',
      clear: 'donorsCreateClear'
    },
    update: {
      success: 'donorsUpdateSuccess',
      error: 'donorsUpdateError',
      clear: 'donorsUpdateClear'
    },
    delete: {
      success: 'donorsDeleteSuccess',
      error: 'donorsDeleteError',
      clear: 'donorsDeleteClear'
    },
    fetch: {
      success: 'donorsFetchSuccess',
      error: 'donorsFetchError'
    },
    byId: {
      fetch: {
        success: 'donorsByIdFetchSuccess',
        error: 'donorsByIdFetchError',
        clear: 'donorsByIdFetchClear'
      }
    },
    receivedNew: 'donorsReceivedNew',
    receivedUpdated: 'donorsReceivedUpdated',
    receivedDeleted: 'donorsReceivedDeleted'
  }
};

export default actionTypes;
