import actionTypes from '~/actionTypes';
import Api from '~/utils/Api';

export function createDonor(data) {
  return (dispatch) => {
    Api
      .post('/donors', data)
      .then((res) => {
        dispatch({
          type: actionTypes.donors.create.success,
          payload: {
            donor: res.data.donor
          }
        });
      })
      .catch((err) => {
        dispatch({
          type: actionTypes.donors.create.error,
          payload: {
            errors: err.data.errors
          }
        });
      })
    ;
  };
}

export function fetchDonors(coords={}) {
  return (dispatch) => {
    Api
      .get('/donors', coords)
      .then((res) => {
        dispatch({
          type: actionTypes.donors.fetch.success,
          payload: {
            donors: res.data.donors
          }
        });
      })
    ;
  };
}

export function fetchDonorById(donorId) {
  return (dispatch) => {
    Api
      .get(`/donors/${donorId}`)
      .then((res) => {
        dispatch({
          type: actionTypes.donors.byId.fetch.success,
          payload: {
            donor: res.data
          }
        });
      })
      .catch((err) => {
        dispatch({
          type: actionTypes.donors.byId.fetch.error,
          payload: {
            status: err.status,
            error: err.data.msg
          }
        });
      })
    ;
  };
}

export function updateDonor(donorId, data) {
  return (dispatch) => {
    Api
      .put(`/donors/${donorId}`, data)
      .then((res) => {
        dispatch({
          type: actionTypes.donors.update.success,
          payload: {
            donor: res.data.donor
          }
        });
      })
      .catch((err) => {
        dispatch({
          type: actionTypes.donors.update.error,
          payload: {
            errors: err.data.errors
          }
        });
      })
    ;
  };
}

export function deleteDonorById(donorId) {
  return (dispatch) => {
    Api
      .delete(`/donors/${donorId}`)
      .then((res) => {
        dispatch({
          type: actionTypes.donors.delete.success,
          payload: {
            donor: res.data.donor
          }
        });
      })
    ;
  };
}

export function receivedNewDonor(donor) {
  return {
    type: actionTypes.donors.receivedNew,
    payload: {
      donor
    }
  };
}

export function receivedUpdatedDonor(donor) {
  return {
    type: actionTypes.donors.receivedUpdated,
    payload: {
      donor
    }
  };
}

export function receivedDeletedDonor(donor) {
  return {
    type: actionTypes.donors.receivedDeleted,
    payload: {
      donor
    }
  };
}

export function clearCreate() {
  return {
    type: actionTypes.donors.create.clear
  };
}

export function clearUpdate() {
  return {
    type: actionTypes.donors.update.clear
  };
}

export function clearFetchById() {
  return {
    type: actionTypes.donors.byId.fetch.clear
  };
}

