import actionTypes from '~/actionTypes';
// import Api from '~/utils/Api';

export function notify(msg) {
  return {
    type: actionTypes.app.notify,
    payload: {msg}
  };
}

export function clearNotification() {
  return {
    type: actionTypes.app.clearNotification
  };
}

