import actionTypes from '../actionTypes';

export function donor(state={}, action) {
  switch (action.type) {
    case actionTypes.donors.create.success:
      return {...state, createSuccess: true, createdInstance: action.payload.donor};

    case actionTypes.donors.create.error:
      return {...state, createErrors: action.payload.errors, createSuccess: false};

    case actionTypes.donors.create.clear:
      return {...state, createErrors: {}, createSuccess: false, createdInstance: {}};

    case actionTypes.donors.update.success:
      return {...state, updateSuccess: true, updatedInstance: action.payload.donor};

    case actionTypes.donors.update.error:
      return {...state, updateErrors: action.payload.errors, updateSuccess: false};

    case actionTypes.donors.update.clear:
      return {...state, updateErrors: {}, updateSuccess: false, updatedInstance: {}};

    case actionTypes.donors.delete.success: {
      return {...state, deleteSuccess: true, deletedInstance: action.payload.donor};
    }

    case actionTypes.donors.delete.clear:
      return {...state, deleteSuccess: undefined};

    case actionTypes.donors.fetch.success:
      return {...state, all: action.payload.donors};

    case actionTypes.donors.byId.fetch.success: {
      const oldDonors = state.all ? state.all.slice() : [];
      oldDonors.push(action.payload.donor);
      return {...state, all: oldDonors};
    }

    case actionTypes.donors.byId.fetch.error:
      return {...state, fetchByIdError: action.payload.error};

    case actionTypes.donors.byId.fetch.clear:
      return {...state, fetchByIdError: undefined};

    case actionTypes.donors.receivedNew: {
      const oldDonors = state.all ? state.all.slice() : [];
      oldDonors.push(action.payload.donor);
      return {...state, all: oldDonors};
    }

    case actionTypes.donors.receivedUpdated: {
      const oldDonors = state.all ? state.all.slice() : [];

      const updateIndex = oldDonors
        .map((d, i) => d._id === action.payload.donor._id ? i : null)
        .filter((indices) => indices !== null)
        .pop()
      ;
      if (updateIndex !== undefined) {
        // updated user in scene, delete existing
        oldDonors.splice(updateIndex, 1);
      }
      oldDonors.push(action.payload.donor);
      return {...state, all: oldDonors};
    }

    case actionTypes.donors.receivedDeleted: {
      const oldDonors = state.all ? state.all.slice() : [];
      const deleteIndex = oldDonors
        .map((d, i) => d._id === action.payload.donor._id ? i : null)
        .filter((indices) => indices !== null)
        .pop()
      ;

      if (deleteIndex !== undefined) {
        // deleted user in scene, delete existing
        oldDonors.splice(deleteIndex, 1);
      }

      return {...state, all: oldDonors};
    }
  }

  return state;
}
