import actionTypes from '../actionTypes';

export function app(state={}, action) {
  switch (action.type) {
    case actionTypes.app.notify:
      return {...state, notification: action.payload.msg};

    case actionTypes.app.clearNotification:
      return {...state, notification: undefined};

  }

  return state;
}
