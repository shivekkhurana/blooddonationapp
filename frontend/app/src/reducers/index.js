import {combineReducers} from 'redux';
import {donor} from '~/reducers/donor';
import {app} from '~/reducers/app';

const rootReducer = combineReducers({
  donor,
  app
});

export default rootReducer;
