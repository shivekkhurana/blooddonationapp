const isWindowEsriSet = () => {
  if (window.esri && window.esri.Map) {
    return true;
  } else {
    return false;
  }
};

const Esri = new Promise((resolve) => {
  const poller = setInterval(() => {
    if (isWindowEsriSet()) {
      clearInterval(poller);
      resolve(window.esri);
    }
  }, 100);
});

export default Esri;
