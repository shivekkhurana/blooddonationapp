import {frontendBase} from 'config';

const DomUtil = {
  resourceLink: (path) => {
    return `${frontendBase}/assets${path}`;
  }
};

export default DomUtil;
