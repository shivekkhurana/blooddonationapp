import axios from 'axios';
import {apiBase} from 'config';
import {assign} from 'lodash';

const requestConfig = {
  headers: {
    'auth-token': localStorage.getItem('token')
  }
};

const Api = {
  get: (endpoint, data) => {
    const mRequestConfig = assign({}, requestConfig, {params: data});
    return axios.get(apiBase + endpoint, mRequestConfig);
  },
  post: (endpoint, data={}) => {
    return axios.post(apiBase + endpoint, data, requestConfig);
  },
  put: (endpoint, data={}) => {
    return axios.put(apiBase + endpoint, data, requestConfig);
  },
  delete: (endpoint) => {
    return axios.delete(apiBase + endpoint, requestConfig);
  }
};

export default Api;
