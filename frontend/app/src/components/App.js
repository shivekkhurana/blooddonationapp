import React, {PropTypes} from 'react';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {red400, grey800, indigoA200} from 'material-ui/styles/colors';
import Notification from '~/components/shared/Notification';

import rootReducer from '~/reducers';
import AppStyle from '~/style/components/App.styl';

injectTapEventPlugin(); // required by material-ui

const muiTheme = getMuiTheme({
  fontFamily: 'Lato, San Francisco, sans-serif',
  palette: {
    primary1Color: red400,
    accent1Color: indigoA200,
    textColor: grey800
  }
});

let store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

class App extends React.Component {
  render() {
    return <div className="App">
      <Provider store={store}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div style={{height: '100%'}}>
            {this.props.children}
            <Notification />
          </div>
        </MuiThemeProvider>
      </Provider>
    </div>;
  }
}

App.propTypes = {
  children: PropTypes.object
};

export default App;
