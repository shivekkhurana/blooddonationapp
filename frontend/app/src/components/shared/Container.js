import React from 'react';

class Container extends React.Component {
  render() {
    return <div
      className={`${this.props.className !== undefined ? this.props.className : ''} container`}
      style={{marginTop: '100px'}}
    >
      {/*
        Need to be applied only once on the page level. Helps give padding and full width for rows to work properly.
      */}
      {this.props.children}
    </div>;
  }
}

export default Container;
