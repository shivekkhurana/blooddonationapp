import React from 'react';
import {connect} from 'react-redux';
import Snackbar from 'material-ui/Snackbar';

import {clearNotification} from '~/actions/appActions';

class Notification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  componentWillUnmount() {
    this.props.dispatch(clearNotification());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.notification && newProps.notification !== this.props.notification) {
      this.setState({
        open: true
      });
    }
  }

  render() {
    return <div>
      <Snackbar
        open={this.state.open}
        message={this.props.notification}
        autoHideDuration={4000}
        onRequestClose={() => this.setState({open: false})}
      />
    </div>;
  }
}

Notification.propTypes = {
  notification: React.PropTypes.string
};

Notification.defaultProps = {
  notification: ''
};

const mapStateToProps = (state) => {
  return {
    notification: state.app.notification
  };
};

export default connect(mapStateToProps)(Notification);
