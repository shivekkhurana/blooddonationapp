import React from 'react';
import {connect} from 'react-redux';
import isEqual from 'lodash/isEqual';
import differenceWith from 'lodash/differenceWith';

import EsriLoader from '~/utils/EsriLoader';
import {fetchDonors} from '~/actions/donorActions';
import DonorShow from '~/components/donor/Show';
import Dialog from 'material-ui/Dialog';
import MapStyle from '~/style/components/shared/Map.styl';

const dojo = window.dojo;

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapLoaded: false,
      isDonorInfoModalOpen: false,
      selectedDonor: {}
    };
  }

  componentWillMount() {
    this.props.dispatch(fetchDonors());
  }

  showDetails = (donor) => {
    this.setState({
      selectedDonor: donor,
      isDonorInfoModalOpen: true
    });
  }

  zoomToLocation = (location) => {
    const Esri = window.esri;
    this.setState({
      location
    });
    const pt = Esri.getometry.Point(location.coords.longitude, location.coords.latitude);
    this.state.map.addGraphic(pt);
    this.state.map.centerAndZoom(pt, 12);
  }

  async componentDidMount() {
    const Esri = await EsriLoader;

    const map = Esri.Map('map', {
      center: [this.props.geo.lat, this.props.geo.lng],
      zoom: 8,
      basemap: 'topo'
    });

    map.on('load', () => {
      this.setState({
        mapLoaded: true,
        map,
        Esri
      });
    });
    this.handleExtentChange();

    dojo.connect(map, 'onExtentChange', (e) => {
      this.handleExtentChange();
    });
  }

  compareExtents = (e1, e2) => {
    if (!e1 || !e2) {
      return false;
    }

    const coords1 = [e1.xmin, e1.ymin, e1.xmax, e1.ymax];
    const coords2 = [e2.xmin, e2.ymin, e2.xmax, e2.ymax];
    return coords1[0] === coords2[0] && coords1[1] === coords2[1] && coords1[2] === coords2[2] && coords1[3] === coords2[3];
  }

  handleExtentChange = () => {
    if (this.state.map) {
      const ex = this.state.map.geographicExtent;
      const coords = [ex.xmin, ex.ymin, ex.xmax, ex.ymax];
      this.setState({extent: ex});
      this.props.dispatch(fetchDonors(coords));
    } else {
      setTimeout(() => {
        this.handleExtentChange();
      }, 200);
    }
  }

  addPointers = (donors) => {
    if (this.state.mapLoaded) {
      const Esri = this.state.Esri;
      this.state.map.graphics.clear();
      donors.forEach((donor) => {
        const p = Esri.geometry.Point(donor.geo.lng, donor.geo.lat);
        const symbol = Esri.symbol.PictureMarkerSymbol({
          'angle': 0,
          'xoffset': 0,
          'yoffset': 12,
          'type': 'esriPMS',
          'url': 'http://static.arcgis.com/images/Symbols/Basic/RedStickpin.png',
          'contentType': 'image/png',
          'width': 60,
          'height': 60
        });
        const g = Esri.Graphic(p, symbol, donor);
        this.state.map.graphics.add(g);
      });

      const lastDonor = donors.slice(-1).pop();
      if (lastDonor) {
        this.state.map.centerAt(Esri.geometry.Point(lastDonor.geo.lng, lastDonor.geo.lat));
        this.handleExtentChange();
      }

      dojo.connect(this.state.map.graphics, 'onClick', (e) => {
        this.setState({
          selectedDonor: e.graphic.attributes,
          isDonorInfoModalOpen: true
        });
      });

    } else {
      setTimeout(() => {
        this.addPointers(donors);
      }, 200);
    }
  }
  async componentWillReceiveProps(newProps) {
    const diff = differenceWith(newProps.donors, this.props.donors, isEqual);
    const deleteDiff = differenceWith(this.props.donors, newProps.donors, isEqual); 

    if (diff.length > 0 || deleteDiff.length > 0) {
      this.addPointers(newProps.donors);
    }
  }

  render() {
    return <div className='Map'>
      <div id='map'></div>
      <Dialog
        modal={false}
        open={this.state.isDonorInfoModalOpen}
        autoScrollBodyContent={true}
        contentStyle={{width: '40%'}}
        onRequestClose={() => this.setState({isDonorInfoModalOpen: !this.state.isDonorInfoModalOpen})}
      >
        <DonorShow donor={this.state.selectedDonor} />
      </Dialog>
    </div>;
  }
}

Map.propTypes = {
  geo: React.PropTypes.object,
  donors: React.PropTypes.array
};

Map.defaultProps = {
  geo: {lng: 28.654973, lat: 77.132105},
  donors: []
};

const mapStateToProps = (state) => {
  return {
    donors: state.donor ? state.donor.all : []
  };
};

export default connect(mapStateToProps)(Map);
