import React from 'react';
import {connect} from 'react-redux';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import DonorsIcon from 'material-ui/svg-icons/action/face';
import DonateIcon from 'material-ui/svg-icons/action/favorite';
import Avatar from 'material-ui/Avatar';
import {
blue300,
indigo900,
orange200,
deepOrange300,
pink400,
purple500
} from 'material-ui/styles/colors';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

import DonorCreate from '~/components/donor/Create';

import TopBarStyle from '~/style/components/shared/nav/TopBar.styl';

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDonorCreateModalOpen: false,
      isShowAllDonorsModalOpen: false
    };
  }

  getRandomColor = () => {
    const colors = [blue300, indigo900, orange200, deepOrange300, pink400, purple500];
    return colors[Math.floor(Math.random()*colors.length)];
  }

  render() {
    return <div>
      <AppBar
        className="TopBar"
        title="Lifeline by Crossover"
        showMenuIconButton={false}
      >
        <div className="links">
          <FlatButton
            label="Donate"
            onTouchTap={() => this.setState({isDonorCreateModalOpen: !this.state.isDonorCreateModalOpen})}
            icon={<DonateIcon color='#fff' />}
          />
          <FlatButton
            label={`${this.props.donorCount} Donors Registered`}
            icon={<DonorsIcon color='#fff' />}
            onTouchTap={() => this.setState({isShowAllDonorsModalOpen: !this.state.isShowAllDonorsModalOpen})}
          />
        </div>

        <Dialog
          title="Become a Donor."
          modal={false}
          open={this.state.isDonorCreateModalOpen}
          autoScrollBodyContent={true}
          onRequestClose={() => this.setState({isDonorCreateModalOpen: !this.state.isDonorCreateModalOpen})}
        >
          <DonorCreate />
        </Dialog>

        <Dialog
          title="Registered Donors"
          modal={false}
          open={this.state.isShowAllDonorsModalOpen}
          autoScrollBodyContent={true}
          onRequestClose={() => this.setState({isShowAllDonorsModalOpen: !this.state.isShowAllDonorsModalOpen})}
        >
          {
            this.props.donors.length ?
              this.props.donors.map((d, i) => {
                const bgColor = this.getRandomColor();
                return <div key={i}>
                  <Card className='card'>
                    <CardHeader
                      title={d.firstName + ' ' + d.lastName}
                      subtitle={`A donor of blood group ${d.bloodGroup} is listed. Click to view info.`}
                      avatar={
                        <Avatar
                          color='#fff'
                          backgroundColor={bgColor}
                          size={30}
                        >
                          {d.bloodGroup}
                        </Avatar>
                      }
                      actAsExpander={true}
                      showExpandableButton={true}
                    />
                    <CardText expandable={true}>
                      {d.contactNumber} - {d.email}
                    </CardText>
                  </Card>
                </div>;
              }) :
              <div>This section shows all registered donors. Add a donor to continue</div>
          }
        </Dialog>

      </AppBar>
    </div>;
  }
}

TopBar.propTypes = {
  donors: React.PropTypes.array
};

TopBar.defaultProps = {
  donors: []
};

const mapStateToProps = (state) => {
  return {
    donorCount: state.donor.all ? state.donor.all.length : 0,
    donors: state.donor.all ? state.donor.all : []
  };
};

export default connect(mapStateToProps)(TopBar);
