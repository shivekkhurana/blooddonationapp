import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import PhoneIcon from 'material-ui/svg-icons/maps/local-phone';
import ShowStyle from '~/style/components/donor/Show.styl';

class Show extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDonorInfoVisible: false
    };
  }

  componentWillMount() {
  }

  render() {
    const donor = this.props.donor;
    return <div className="Show">
      <div className="row">
        <div className="col-sm-3">
          <div className="box">
            <div className="bloodGroup">
              {donor.bloodGroup}
            </div>
          </div>
        </div>
        <div className="col-sm-5 col-sm-offset-4">
          <div className="box">
            {
              this.state.isDonorInfoVisible ?
                <div>
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="box">
                        <div className='key'>Name</div>
                        <div className='value'>{donor.firstName + ' ' + donor.lastName}</div>

                        <div className='key'>Email</div>
                        <div className='value'>{donor.email}</div>

                        <div className='key'>Contact Number</div>
                        <div className='value'>{donor.contactNumber}</div>

                        <RaisedButton
                          secondary={true}
                          label='Hide contact info'
                          onTouchTap={() => this.setState({isDonorInfoVisible: false})}
                          icon={<PhoneIcon color='#fff'/>}
                        />
                      </div>
                    </div>
                  </div>
                </div>:
                <RaisedButton
                  primary={true}
                  className='showInfoButton'
                  label='Show contact info'
                  onTouchTap={() => this.setState({isDonorInfoVisible: true})}
                  icon={<PhoneIcon color='#fff'/>}
                />
            }
          </div>
        </div>
      </div>
    </div>;
  }
}

Show.propTypes = {
  donor: React.PropTypes.object
};

Show.defaulProps = {
  donor: {}
};

export default Show;
