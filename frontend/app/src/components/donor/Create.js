import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import {createDonor, clearCreate} from '~/actions/donorActions';
import {notify} from '~/actions/appActions';
import CreateStyle from '~/style/components/donor/Create.styl';

class DonorCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bloodGroupValue: 1,
      addLocationLabel: 'Add your location'
    };
  }

  componentWillUnmount() {
    this.props.dispatch(clearCreate());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors['geo.lat']) {
      this.props.dispatch(notify('Please allow us access to your location.'));
    }
  }

  getBloodGroup = () => {
    switch (this.state.bloodGroupValue) {
      case 1:
        return 'O-';
      case 2:
        return 'O+';
      case 3:
        return 'A-';
      case 4:
        return 'A+';
      case 5:
        return 'B-';
      case 6:
        return 'B+';
      case 7:
        return 'AB-';
      case 8:
        return 'AB+';
      default:
        return null;
    }
  }

  promptLocationRequest = () => {
    if (navigator.geolocation) {
      this.setState({
        addLocationLabel: 'Loading ...'
      });
      navigator.geolocation.getCurrentPosition((location) => {
        this.setState({
          location
        });
      }, this.locationError);
    } else {
      this.props.dispatch(notify('Browser doesn\'t support Geolocation.'));
    }
  }

  locationError = (error) => {
    this.setState({
      addLocationLabel: 'Please contact support. An error has occured.'
    });
    switch (error.code) {
      case error.PERMISSION_DENIED:
        this.props.dispatch(notify('Please add location to help patients faster.'));
        break;

      case error.POSITION_UNAVAILABLE:
        this.props.dispatch(notify('Current location not available.'));
        break;

      case error.TIMEOUT:
        this.props.dispatch(notify('Max wait time exceeded.'));
        break;

      default:
        this.props.dispatch(notify('A cheetah ate our code (Unknown Error)'));
        break;
    }
  }

  onCreateDonorClick = async () => {
    const data = {
      firstName: this.refs.firstName.input.value,
      lastName: this.refs.lastName.input.value,
      email: this.refs.email.input.value,
      contactNumber: this.refs.contactNumber.input.value,
      bloodGroup: this.getBloodGroup(),
      geo: this.state.location ?
        {lat: this.state.location.coords.latitude, lng: this.state.location.coords.longitude} : {}
    };
    this.props.dispatch(clearCreate());
    this.props.dispatch(createDonor(data));
  }

  render() {
    return <div className='DonorCreate'>
      {
        this.props.createSuccess ?
          <div>
            <h2>Your listing has been added to database</h2>
            <div>
              To update or delete your listing, please visit
              &nbsp;<Link to={`/donor/update/${this.props.donor._id}`} target="_blank">this page</Link>.
            </div>
          </div>:
          <div className="form">
            <div className='row'>
              <div className='col-sm-6'>
                <div className='box'>
                  <TextField
                    floatingLabelText='First Name'
                    ref='firstName'
                    className='field'
                    errorText={this.props.errors ? this.props.errors.firstName : ''}
                  >
                  </TextField>
                </div>
              </div>

              <div className='col-sm-6'>
                <div className='box'>
                  <TextField
                    floatingLabelText='Last Name'
                    ref='lastName'
                    className='field'
                    errorText={this.props.errors ? this.props.errors.lastName : ''}
                  >
                  </TextField>
                </div>
              </div>
            </div>

            <div className='row'>
              <div className='col-sm-6'>
                <div className='box'>
                  <TextField
                    floatingLabelText='Email'
                    ref='email'
                    className='field'
                    errorText={this.props.errors ? this.props.errors.email : ''}
                  >
                  </TextField>
                </div>
              </div>

              <div className='col-sm-6'>
                <div className='box'>
                  <TextField
                    floatingLabelText='Contact Number'
                    ref='contactNumber'
                    className='field'
                    errorText={this.props.errors ? this.props.errors.contactNumber : ''}
                  >
                  </TextField>
                </div>
              </div>
            </div>

            <div className='row'>
              <div className='col-sm-12'>
                <div className='box'>
                  <SelectField
                    value={this.state.bloodGroupValue}
                    onChange={(event, index, value) => this.setState({bloodGroupValue: value})}
                    ref='bloodGroup'
                    floatingLabelText='Blood Group '
                    className='field bloodGroup'
                  >
                    <MenuItem value={1} primaryText='O-' />
                    <MenuItem value={2} primaryText='O+' />
                    <MenuItem value={3} primaryText='A-' />
                    <MenuItem value={4} primaryText='A+' />
                    <MenuItem value={5} primaryText='B-' />
                    <MenuItem value={6} primaryText='B+' />
                    <MenuItem value={7} primaryText='AB-' />
                    <MenuItem value={8} primaryText='AB+' />
                  </SelectField>
                </div>
              </div>
            </div>

            <div className="row">
              <div className='col-sm-12'>
                <div className='box'>
                  {
                    this.state.location ?
                      <div>Location added successfully.</div> :
                      <RaisedButton
                        label={this.state.addLocationLabel}
                        className='cta'
                        onTouchTap={this.promptLocationRequest}
                      />
                  }
                </div>
              </div>
            </div>

            <div className="row">
              <div className='col-sm-12'>
                <div className='box'>
                  <RaisedButton
                    primary={true}
                    label='Submit'
                    className='cta'
                    onTouchTap={this.onCreateDonorClick}
                  />
                </div>
              </div>
            </div>
          </div>
      }
    </div>;
  }
}

DonorCreate.propTypes = {
  errors: React.PropTypes.object,
  createSuccess: React.PropTypes.bool
};

DonorCreate.defaultProps = {
  errors: {},
  createSuccess: false
};

const mapStateToProps = (state) => {
  return {
    errors: state.donor.createErrors,
    createSuccess: state.donor.createSuccess,
    donor: state.donor.createdInstance
  };
};

export default connect(mapStateToProps)(DonorCreate);
