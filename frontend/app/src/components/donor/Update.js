import React from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import HomeIcon from 'material-ui/svg-icons/action/home';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {red50} from 'material-ui/styles/colors';

import {fetchDonorById, clearFetchById, clearUpdate, updateDonor, deleteDonorById} from '~/actions/donorActions';
import {notify} from '~/actions/appActions';
import UpdateStyle from '~/style/components/donor/Update.styl';

const TopBar = () => (
  <div>
    <AppBar
      className="TopBar"
      style={{marginTop: 0, position: 'fixed', top: 0}}
      title="Lifeline by Crossover"
      showMenuIconButton={false}
    >
      <div className="links">
        <Link to='/'><FlatButton
          label="Home"
          icon={<HomeIcon color='#fff' />}
        /></Link>
      </div>
    </AppBar>
  </div>
);

class Update extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      donorExists: false,
      doesNotExistMessage: 'Loading ...',
      deleteButtonLabel: 'Delete'
    };
  }

  componentWillMount() {
    if (!Object.keys(this.props.donor).length) {
      this.props.dispatch(fetchDonorById(this.props.routeParams.donorId));
    }
  }

  componentWillUnmount() {
    this.props.dispatch(clearFetchById());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.fetchByIdError) {
      this.setState({
        doesNotExistMessage: 'This donor does not exist. Please double check the url.'
      });

      this.props.dispatch(notify(newProps.fetchByIdError));
      this.props.dispatch(clearFetchById());
    }

    if (newProps.donor && Object.keys(newProps.donor).length) {
      this.setState({
        donorExists: true,
        bloodGroupValue: this.getBloodGroupValue(newProps.donor.bloodGroup)
      });
    }

    if (newProps.updateSuccess) {
      this.props.dispatch(notify('Listing updated successfully.'));
    }

    if (newProps.deleteSuccess) {
      this.setState({
        donorExists: false
      });
      this.props.dispatch(notify('Listing deleted successfully.'));
      setTimeout(() => {
        this.props.router.push('/');
      }, 1000)
    } else {
      this.setState({
        deleteButtonLabel: 'Delete'
      })
    }
  }

  onUpdateDonorClick = () => {
    const data = {
      firstName: this.refs.firstName.input.value,
      lastName: this.refs.lastName.input.value,
      email: this.refs.email.input.value,
      contactNumber: this.refs.contactNumber.input.value,
      bloodGroup: this.getBloodGroup(),
      geo: {lat: this.refs.lat.input.value, lng: this.refs.lng.input.value}
    };
    this.props.dispatch(clearUpdate());
    this.props.dispatch(updateDonor(this.props.donor._id, data));
  }

  onDeleteDonorClick = () => {
    this.setState({
      deleteButtonLabel: 'Working ...'
    });
    this.props.dispatch(deleteDonorById(this.props.donor._id));
  }

  getBloodGroupValue = (bloodGroup) => {
    switch (bloodGroup) {
      case 'O-':
        return 1;
      case 'O+':
        return 2;
      case 'A-':
        return 3;
      case 'A+':
        return 4;
      case 'B-':
        return 5;
      case 'B+':
        return 6;
      case 'AB-':
        return 7;
      case 'AB+':
        return 8;
      default:
        return 1;
    }
  }

  getBloodGroup = () => {
    switch (this.state.bloodGroupValue) {
      case 1:
        return 'O-';
      case 2:
        return 'O+';
      case 3:
        return 'A-';
      case 4:
        return 'A+';
      case 5:
        return 'B-';
      case 6:
        return 'B+';
      case 7:
        return 'AB-';
      case 8:
        return 'AB+';
      default:
        return null;
    }
  }

  render() {
    const donor = this.props.donor;
    return <div className="Update">
      <TopBar />
      <div className="content">
        <div className="row">
          <div className="col-sm-8 col-sm-offset-2">
            <div className="box">
              <h2>Update Listing</h2>

              {
                !this.state.donorExists ?
                  <div>{this.state.doesNotExistMessage}</div>:
                  <div>
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='First Name'
                            ref='firstName'
                            defaultValue={donor.firstName}
                            className='field'
                            errorText={this.props.errors ? this.props.errors.firstName : ''}
                          />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='Last Name'
                            ref='lastName'
                            defaultValue={donor.lastName}
                            className='field'
                            errorText={this.props.errors ? this.props.errors.lastName : ''}
                          />
                        </div>
                      </div>
                    </div>
                
                    <div className="row">
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='Email'
                            ref='email'
                            className='field'
                            defaultValue={donor.email}
                            errorText={this.props.errors ? this.props.errors.email : ''}
                          />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='Contact Number'
                            ref='contactNumber'
                            className='field'
                            defaultValue={donor.contactNumber}
                            errorText={this.props.errors ? this.props.errors.contactNumber : ''}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-sm-12">
                        <div className="box">
                          <SelectField
                            value={this.state.bloodGroupValue}
                            onChange={(event, index, value) => this.setState({bloodGroupValue: value})}
                            ref='bloodGroup'
                            floatingLabelText='Blood Group '
                            className='field bloodGroup'
                          >
                            <MenuItem value={1} primaryText='O-' />
                            <MenuItem value={2} primaryText='O+' />
                            <MenuItem value={3} primaryText='A-' />
                            <MenuItem value={4} primaryText='A+' />
                            <MenuItem value={5} primaryText='B-' />
                            <MenuItem value={6} primaryText='B+' />
                            <MenuItem value={7} primaryText='AB-' />
                            <MenuItem value={8} primaryText='AB+' />
                          </SelectField>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='Latitude'
                            ref='lat'
                            className='field'
                            defaultValue={donor.geo ? donor.geo.lat : ''}
                            errorText={this.props.errors ? this.props.errors['geo.lat'] : ''}
                          />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="box">
                          <TextField
                            floatingLabelText='Longitude'
                            ref='lng'
                            className='field'
                            defaultValue={donor.geo ? donor.geo.lng : ''}
                            errorText={this.props.errors ? this.props.errors['geo.lng'] : ''}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-sm-12">
                        <div className="box">
                          <RaisedButton
                            primary={true}
                            label='Update'
                            className='cta'
                            onTouchTap={this.onUpdateDonorClick}
                          />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-sm-12">
                        <div className="box deleteButton">
                          <h2>Delete this listing ?</h2>
                          Deleting this is a non reversible process. Data once deleted is lost forever. Proceed with caution.
                          <RaisedButton
                            label={this.state.deleteButtonLabel}
                            backgroundColor={red50}
                            className='cta'
                            onTouchTap={this.onDeleteDonorClick}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
              }
            </div>
          </div>
        </div>
      </div>
    </div>;
  }
}

Update.propTypes = {
  donor: React.PropTypes.object
};

Update.defaulProps = {
  donor: {},
  updateErrors: [],
  updateSuccess: false
};

const mapStateToProps = (state, componentProps) => {
  return {
    donor: state.donor.updatedInstance ?
      state.donor.updatedInstance :
      state.donor.all ?
        state.donor.all.filter((d) => d._id === componentProps.routeParams.donorId).slice(-1).pop() :
        {},
    fetchByIdError: state.donor.fetchByIdError,
    errors: state.donor.updateErrors,
    updateSuccess: state.donor.updateSuccess,
    deleteSuccess: state.donor.deleteSuccess
  };
};

export default connect(mapStateToProps)(withRouter(Update));
