import React from 'react';
import io from 'socket.io-client';
import {connect} from 'react-redux';

import TopBar from '~/components/shared/nav/TopBar';
import {notify} from '~/actions/appActions';
import {apiBase} from 'config';
import {receivedNewDonor, receivedUpdatedDonor, receivedDeletedDonor} from '~/actions/donorActions';
import Map from '~/components/shared/Map';
import LandingStyle from '~/style/components/Landing.styl';

class Landing extends React.Component {
  constructor(props) {
    super(props);
    const socket = io.connect(apiBase);
    for (const [key, handler] of Object.entries(this.socketHandlers())) {
      socket.on(key, handler);
    }
    this.state = {socket};
  }

  socketHandlers = () => {
    return {
      notification: (data) => {
        this.props.dispatch(notify(data.msg));
      },
      newDonorAdded: (donor) => {
        this.props.dispatch(notify(`A new donor of blood group ${donor.bloodGroup} has been added.`));
        this.props.dispatch(receivedNewDonor(donor));
      },
      donorUpdated: (donor) => {
        this.props.dispatch(notify(`A donor of blood group ${donor.bloodGroup} updated their credentials.`));
        this.props.dispatch(receivedUpdatedDonor(donor));
      },
      donorDeleted: (donor) => {
        this.props.dispatch(notify(`A donor of blood group ${donor.bloodGroup} deleted their credentials.`));
        this.props.dispatch(receivedDeletedDonor(donor));
      }
    };
  }

  componentWillMount() {
  }

  render() {
    return <div className='Landing'>
      <TopBar />
      <Map />
    </div>;
  }
}

Landing.propTypes = {
};

Landing.defaultProps = {
};

const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps)(Landing);
