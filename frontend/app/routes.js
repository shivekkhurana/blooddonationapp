import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import App from '~/components/App';
import Landing from '~/components/Landing';
import DonorUpdate from '~/components/donor/Update';

const Routes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Landing} />
      <Route path="/donor/update/:donorId" component={DonorUpdate} />
    </Route>
  </Router>
);

export function serve() {
  render(<Routes/>, document.getElementById('container'));
}
