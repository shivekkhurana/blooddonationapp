Frontend
=================

- `$ npm run build`
- `$ npm run serve`


## Components Organization

Components are organized as per routes.js.
- If there is a route such as `/foo`, then a component name `Foo.js` is expected in `~/components`.

- If there is a route such as `/foo` & `/foo/bar`, then a component name `index.js` is expected in `~/components/foo`
  to power `/foo` route and `Bar.js` (again in `~/components/foo`) to power `/foo/bar` route.
