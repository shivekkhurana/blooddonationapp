const dev = {
  apiBase: 'http://localhost:3000',
  frontendBase: 'http://localhost:5000'
};

const prod = {
  apiBase: 'http://api.crossover.cf',
  frontendBase: 'http://crossover.cf'
};

let applicableConfig;

if (process.env.NODE_ENV === 'production') {
  applicableConfig = prod;
} else {
  applicableConfig = dev;
}

export default applicableConfig;

